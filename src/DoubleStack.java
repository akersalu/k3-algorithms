
//TODO: interpret method can be optimized / made smaller

import java.util.LinkedList;
import java.util.StringTokenizer;

public class DoubleStack {
   //operand - 1,2,3
   //operator - +,-,*,/
   public static void main (String[] argum) {

//      DoubleStack uusStack = new DoubleStack();
//      uusStack.push(5.5);
//      uusStack.push(6.5);
//      uusStack.push(7.5);
//      System.out.println(uusStack);

      String a = "2 5 SWAP -";
      String b = " 5 9 6 ROT - +";
      String c = "2 5 9 ROT + SWAP -";
      String uuf = "2 5 1 - SWAP - 3 +";
      //TESTING THE ERROR HANDLING
      String d = "2 5 SWAP - x";
      String e = "2 5 6 SWAP - 2 + +";
      String f = "35. 10. + -";
      String g = "- 3. 4. 6. -";
      System.out.println(DoubleStack.interpret (a));
      System.out.println(DoubleStack.interpret (b));
      System.out.println(DoubleStack.interpret (c));
      System.out.println(DoubleStack.interpret(uuf));

       //TESTING THE ERROR HANDLING
      //illegal argument
       //System.out.println(DoubleStack.interpret (d));
      //interpret operand last
       //System.out.println(DoubleStack.interpret (e));
      //interpret underflow
       //System.out.println(DoubleStack.interpret (f));
      //inerpret operator first
      //System.out.println(DoubleStack.interpret (g));
   }



   private LinkedList<Double> list;
   private int size; //we start counting the size from 0th index


   DoubleStack() {
      this.list = new LinkedList<Double>();
      this.size = -1;
   }

   @Override //PASS
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clone = new DoubleStack();

      int itemsAmount = this.size;
      while(itemsAmount > -1){

         double doubleToCopyOver = this.list.get(itemsAmount);
         clone.push(doubleToCopyOver);
         itemsAmount--;
      }
      return clone;
   }
   //PASS
   public boolean stEmpty() {
      return (this.list.size() == 0);
   }
   //PASS
   public void push (double a) {
      this.list.push(a);
      this.size++;
   }
   //PASS
   public double pop() {
      this.size--;
      try{
         return this.list.pop();
      } catch(RuntimeException e){
         throw new RuntimeException("No items left to pop from the stack(the stack is empty), underflow.");
      }

   }
   //PASS
   public void op (String s) {

      if (this.size<1){
         throw new RuntimeException("Not enough numeric values in the stack to operate on!");
      }
         double op2 = this.list.pop();
         double op1 = this.list.pop();

      if(s.equals("+"))  {
         this.list.push(op1 + op2);
      } else if(s.equals("-")){
         this.list.push(op1 - op2);
      }else if(s.equals("*")){
         this.list.push(op1 * op2);
      }else if(s.equals("/")){
         this.list.push(op1 / op2);
      }
      else{
         throw new RuntimeException("Wrong operator, user input: " +  s);
      }

   }
   //PASS
   public double tos() {
      if (this.size > -1 ){
         return this.list.peek(); // LÄHEB LÄBI
      } else {
         throw new RuntimeException("Stack is empty!");
      }
   }

   @Override
   public boolean equals (Object o) { //
      int stackSize = this.size;

      if (this == o) return true; //if they refer to the same object
      if (o == null) return false; //if argument is null, return false
      if (this.getClass() != o.getClass()) return false; //if the comparable classes are of different types, return false

      DoubleStack other = (DoubleStack) o; //typecast the incoming object to DoubleStack

      if(this.list.size() != other.list.size()) return false; //if one object is bigger than another

      double originalDouble;
      double otherDouble;

      while (stackSize > -1) {
         //somehow putting these two to if statement changes the outcome, making the results incorrect
         //that is why I keep them here
         originalDouble = this.list.get(stackSize);
         otherDouble = other.list.get(stackSize);
         if(originalDouble != otherDouble) return false;
         stackSize--;
      }

      return true; //if all above is passed then objects have the same content inside but are separate objects
   }

   @Override //PASS - print order is fifo, ehk stack-s top is on the right
   public String toString() {
      int stackSize = this.list.size(); //list.size() method starts counting from 1
      if(this.list.size() < 1) return "Stack is empty.";
      StringBuffer b = new StringBuffer();
      while (stackSize>0){
         b.append((list.get(stackSize-1)) + " "); //stackSize - 1 to get the correct index
         stackSize--;
      }
      return b.toString();
   }

   public static double interpret (String pol) {
      if (pol == null || pol.isEmpty()) throw new RuntimeException("The input field is empty");

      //We clean out the input first we use the string tokenizer method
      StringTokenizer st = new StringTokenizer(pol, " ");
      //initialize a new string where we copy our trimmed elements
      String puhasStr = "";

      //trim and copy elements over to a string
      while(st.hasMoreElements()){
         puhasStr += st.nextToken().trim() + " ";
      }
      //tokenize the string again and get a collection of clean values
      StringTokenizer puhasSt = new StringTokenizer(puhasStr, " ");



      //NII NÜÜD MEIL ON PUHAS KOLLEKTSIOON ANDMEID

      //Test for illegal arguments - like x,z,y,*,! and so on. Let through the string that has only allowed values while
      //moving them to a new LinkedList
      //We create a list from the tokenized correct values
      LinkedList<String> cleanAllowedValues = new LinkedList();
      String potentialAllowedValue = "";

      while (puhasSt.hasMoreElements()){
         potentialAllowedValue = puhasSt.nextToken();

         if(isNumeric(potentialAllowedValue) || isAnyOtherTypeOfAllowedValue(potentialAllowedValue)){
            cleanAllowedValues.addLast(potentialAllowedValue);
         } else {
            throw new RuntimeException("Incorrect arguments given by user.\n User input: " + pol +
                    " Detected wrong argument: " + potentialAllowedValue );
         }
      }

      //We create a new stack to push items we currently process into
      DoubleStack stackToProcess = new DoubleStack();

      //KUI STACKBALANCE JA UNDEFLOW LÄBI EI LÄHE, SIIS kirjutan vaja mineva koodi siia.
      if ( cleanAllowedValues.size() > 1 && isNumeric(cleanAllowedValues.peekLast()) ){
         throw new RuntimeException("Last input value should not be an operand, it sould be an operator.\n" +
                 " User input: " + pol + " Last input value: " + cleanAllowedValues.peekLast());
      }

      while(cleanAllowedValues.size() > 0){
         String poppedVal = cleanAllowedValues.pop();
         //SWAP ÜLESANNE
         if (poppedVal.equals("SWAP") && stackToProcess.list.size() > 1){

            double op1 = stackToProcess.pop();
            double op2 = stackToProcess.pop();
            stackToProcess.push(op1);
            stackToProcess.push(op2);
         }
         //ROT ÜL
         else if (poppedVal.equals("ROT")){
             try{
                 double op1 = stackToProcess.list.get(2);
                 stackToProcess.list.remove(2);
                 stackToProcess.list.push(op1);
             } catch (RuntimeException e){
                 throw new RuntimeException("Cannot rotate less than three operands, current user operands " +
                         "for ROT operation: " + stackToProcess.toString() + " \n Whole user input string: " + pol);
             }


         }//if the current argument is an operand
         else if (isNumeric(poppedVal)){
            stackToProcess.push(Double.parseDouble(poppedVal));
            //if the current argumet is an operator
         } else {
            //try to run the operation with the given operator if it fails then we have underflow
            try{
               stackToProcess.op(poppedVal);
            } catch (RuntimeException e){
               //e.printStackTrace();
               throw new RuntimeException("User input caused underflow and thus is incorrect.\n " +
                       "Check your position and balance of operands and operators. User input: " + pol);
            }
         }
      }

        //Lastly if there is more than one item left in the stack, throw an error
      if(stackToProcess.list.size() > 1) throw new RuntimeException("Too many items left in the stack, " +
              "there should be only one item. User input: " + pol);


      return stackToProcess.pop();
   }


   public static boolean isNumeric (String str) {
      try{
         double d = Double.parseDouble(str);
      } catch (NumberFormatException | NullPointerException nfe ){
         return false;
      }
      return true;
   }

   public static boolean isAnyOtherTypeOfAllowedValue (String str){
      if (str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/") || str.equals("SWAP") || str.equals("ROT")) {
         return true;
      } else return false;
   }

}
